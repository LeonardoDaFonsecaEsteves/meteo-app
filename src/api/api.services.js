import Axios from "axios"

export const Request = async (value) => {
  const res = await Axios.get(
    `https://api.openweathermap.org/data/2.5/weather?q=${value}&units=metric&lang=fr&appid=c43cdb00d4e5df5c5224beeb49b93999`
  )
  return res.data
}
