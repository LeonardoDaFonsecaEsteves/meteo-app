import React from "react"
import { Request } from "../api/api.services"
import ViewHistory from "../components/viewHistory"
import ViewWeather from "../components/viewWeather"

function App(props) {
  const [state, setState] = React.useState("")

  const search = () => {
    if (state) {
      Request(state).then((res) => props.action.search(res))
      props.action.history(state)
      setState("")
    }
  }
  return (
    <div className="App ">
      <input type="text" value={state} onChange={(e) => setState(e.target.value)} />
      <button onClick={() => search()}>Click</button>
      <ViewWeather weather={props.weather} />
      <ViewHistory history={props.history} />
    </div>
  )
}

export default App
