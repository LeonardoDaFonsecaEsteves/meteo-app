import React from "react"
import PropTypes from "prop-types"

const ViewHistory = ({ history }) => {
  return (
    history &&
    history.map((value) => {
      return <li>{value}</li>
    })
  )
}

ViewHistory.propTypes = {
  history: PropTypes.array.isRequired
}

export default ViewHistory
