import React from "react"
import PropTypes from "prop-types"

const ViewWeather = ({ weather }) => {
  return (
    weather && (
      <div>
        <h1>ville {weather.name} </h1>
        <p>pays {weather.sys.country} </p>
        <p>
          leve {weather.sys.sunrise}, couché {weather.sys.sunset} soleil
        </p>
        <p> vents {weather.wind.speed} </p>
        <p>
          Longitude: {weather.coord.lon} / latitude : {weather.coord.lat}{" "}
        </p>
        <p>
          Temp {weather.main.temp} , ressenti {weather.main.feels_like}
        </p>
        <p>
          {weather.main.temp_min} min / {weather.main.temp_max} max
        </p>
        <p>
          pression {weather.main.pressure} / humidite : {weather.main.humidity} max
        </p>
        <img
          src={`http://openweathermap.org/img/w/${weather.weather[0].icon}.png`}
          alt={weather.weather[0].main}
        />
        <p>{weather.weather[0].description}</p>
      </div>
    )
  )
}

ViewWeather.propTypes = {
  weather: PropTypes.shape({
    name: PropTypes.string.isRequired,
    wind: PropTypes.shape({
      speed: PropTypes.number
    }).isRequired,
    sys: PropTypes.shape({
      country: PropTypes.string.isRequired,
      sunrise: PropTypes.number.isRequired,
      sunset: PropTypes.number.isRequired
    }).isRequired,
    coord: PropTypes.shape({
      lon: PropTypes.number.isRequired,
      lat: PropTypes.number.isRequired
    }).isRequired,
    main: PropTypes.shape({
      temp: PropTypes.number.isRequired,
      feels_like: PropTypes.number.isRequired,
      humidity: PropTypes.number.isRequired,
      pressure: PropTypes.number.isRequired,
      temp_max: PropTypes.number.isRequired,
      temp_min: PropTypes.number.isRequired
    }).isRequired
  }).isRequired
}

export default ViewWeather
