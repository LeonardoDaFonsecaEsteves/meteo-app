import { HISTORY, SEARCH } from "./action.type"

export const search = (payload) => {
  return { type: SEARCH, payload }
}
export const history = (payload) => {
  return { type: HISTORY, payload }
}
