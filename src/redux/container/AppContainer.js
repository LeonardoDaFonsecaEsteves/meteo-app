import { connect } from "react-redux"
import App from "../../app/App"
import { search, history } from "../action"

const mapStateToProps = (state) => {
  return { ...state }
}

const mapDispacthToProps = (dispatch) => ({
  action: {
    search: (payload) => dispatch(search(payload)),
    history: (payload) => dispatch(history(payload))
  }
})

const AppContainer = connect(mapStateToProps, mapDispacthToProps)(App)

export default AppContainer
