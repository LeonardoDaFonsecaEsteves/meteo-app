import { HISTORY, SEARCH } from "./action/action.type"

const initialState = {
  history: [],
  weather: null
}

const rootReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case SEARCH:
      return { ...state, weather: action.payload }
    case HISTORY:
      return { ...state, history: [...state.history, action.payload] }
    default:
      return state
  }
}

export default rootReducer
